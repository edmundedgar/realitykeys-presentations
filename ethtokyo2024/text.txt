Homage to Augur

The problem: How do we settle a market?
 Who won the 2020 election?

This is a very fundamental problem that generalizes to 
 How do we make the chain tell the truth about something we can't prove automatically
 "inter-subjective work"
 Enforcing DAO constitutions
 Is this upgrade malicious

Solution 1: Token voting
 -> bribery attacks
 -> 51% attacks
 -> $100 in a box, decentralization makes it worse

THIS IS VERY VERY VERY BAD

Solution 2: Token voting with slashing
 -> bribery attacks intensify
 -> p-epsilon

Solution 3: Add forking
 -> 2 versions of the system
 -> governance token holders choose
 -> Everyone gets paid with the version they want
 -> Nonsense version shouldn't be valuable
 
Add features
 -> add a cost to forking
 -> add an escalation game before we do it
   [explain escalation game]

The hard part: We can't fork our collateral
 -> People want to bet in ETH and DAI
 -> Solution: An economic decision rule

Augur's economic decision rule
 -> Whichever attracts the most tokens is "correct"
 -> Problem: If the token value is lower than assets you can steal, it's rational to attack

Augur approach
 -> Track the value of the token and the open interest
 -> Raise fees if your token value vs open interest goes too low
    -> since revenue is higher, token is worth more, open interest reduces

Result
 -> It worked! Augur didn't missettle or fork!

Why it may not work in the long term
 -> Someone has to hold the token and expect revenue
 -> Raising prices may produce a death spiral not an increase in revenue
 -> Runs on easy mode early on, breaks later
 -> Relies on irrationally exuberant token valuations
 -> This gets worse in the presence of parasites
 -> Cost may just be too high for many use-cases


If a problem is intractable, change the requirements


Make Ethereum fork!
 -> Justin Drake
 -> Martin Koeppelmann long ago
 -> Vitalik says no
 -> Maybe you can still do it if you become TBTF?
 -> Maybe this was secret the goal of Eigenlayer v1

Eigenlayer approach:
 -> Give up the goal of settling the market correctly! Instead, insure you against settling it wrong
 -> If you're a parasite who didn't buy wrong-settlement insurance and it breaks on you, too bad for you
 -> Not at all clear what this looks like in practice for a lot of types of decision
 -> Breaking the system may be profitable, so there's an incentive to make it fork

Backstop approach:
 -> Put stuff on a new layer that we *can* fork
 -> Use a forkable L2 so we don't need our own consensus and we can read L1 trustlessly
 -> Use an economic decision rule only for bridges, and you can use those bridges or not, YMMV

