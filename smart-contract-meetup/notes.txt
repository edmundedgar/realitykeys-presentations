Reality Keys and The Oracle Problem
Smart Contract Oracle, running since February, 2014

The Oracle problem
 Most interesting contracts require knowledge about the external world
 Nodes don't know about the external world
 Ethereum nodes can't even fetch data from the web
 Need someone to tell us what happened

How Reality Keys works
 1) Signed data (very flexible, good for Ethereum)
 2) Conditional private key release (good for Bitcoin)

Trust problems
 What if I take a bribe?
 What if I'm kidnapped?
 What if I'm hacked?

Trust mitigation
 Combine many oracles
 Gnosis is building multiple oracles into their prediction market
 Oraclize does TLS Notarization to help you detect if they lie (but if they do, it's too late...)

Decentralized approaches
 Augur: Everybody votes, punish people who are wrong
 Cool but untested. Specifically problems...
 Slow and expensive (as currently designed)
 Potential bribery problems, probably spreading from indeterminate cases
 Centralization risk as it scales

Martin Koeppelmann of Gnosis
 The Ultimate Oracle
 http://forum.groupgnosis.com/t/the-ultimate-oracle/61
 Use oracles, but have proof-of-stake voting for a fee to overturn their results

Proposed variation:
 Use oracles, but have proof-of-stake voting to declare them dishonest and discard their results
 Alternate governance schemes to proof-of-stake voting

