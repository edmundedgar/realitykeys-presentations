So I'm going to talk about bitcoin. Now bitcoin is a few different things, but I'm going to talk about it at its broadest level, which is a technology for managing digital assets in a peer-to-peer network. 

The most well-known asset it manages is a currency which is also called Bitcoin. There are also other currencies using the same technology. 
There are a bunch of similar currencies like Litecoin and Dogecoin, a gazillion others.

Then there are systems built to track other assets. NameCoin is a P2P version of DNS.
And there are systems for tracking stocks or bonds or all kinds of assets like Ethereum and Colored Coins.


But essentially, this family software is a  system for setting puzzles so that the person who solves that puzzle gets to set another puzzle. 
These puzzles usually involve cryptographic key pairs but they don't have to. But I'll show you what I mean in a minute.

Today I'm going to focus on the transactions that you make when you set and solve puzzles, but before I do that I should tell you how the peer-to-peer networks work. Bear with me if you've heard this before.

[SLIDE]

A p2p network consists of a bunch of nodes know how to talk to each other.
One node will have a connection to a subset of other nodes.
   .
.  .
   .
And each of those nodes will be connected to a bunch more nodes
      .
   .  .
.  .  .
   .  .
      .
So you send out your message to your nodes. If it looks OK to them they relay it to the nodes they're connected to, and so on. 
That way you can typically get a message out across the whole network within a few seconds. So far, it's just like any other p2p network.

[SLIDE]

But the problem here is that if we want to make a digital asset register we need consistency. 
We want everybody to agree on who has which asset.
If somebody sends me an asset and I give them my house in return, I want to be sure that in the future I'll be able to ask any node in the network and they'll all agree that I own the asset.
We don't have to get them to agree instantly, but we do need them to agree eventually. 

So we need some way to get eventually consistent. 
In particular the problem here is making sure we agree on what order these messages were sent in. 
If I send out a message saying that I'm giving my car to Alice, then I send out another message saying I'm giving it to Bob, we need some agreement on who wins. 
Usually that would be Alice because she had it first, and when I gave it to Bob I didn't own it any more. 
But even if the network doesn't know the exact order, it needs to agree on some order.

Every now and again at some random time one of the nodes gathers up all the messages that it's heard, makes sure they're in a consistent state, and puts them together in what we call a block. 
Then it sends the block to the rest of the network.
The other nodes get that block and they keep that as the record of all the messages up to that time. 


But now we have another version of the same problem, because we need to know which block was first. 
From time to time two nodes will make a block at the same time, and those blocks will be inconsistent. 
So we need a tie-breaking method. 

[SLIDE]

And the way that's done in bitcoin is that every time you make a block, you add it to a chain on top of all the other blocks. 
That chain gradually gets longer and longer, and if there's more than one chain floating around here, the nodes will always use the longest chain.
So that's our tie-breaking rule. So sometimes we'll have conflicting blocks floating around for a while, but normally it doesn't go beyond a block or two. 

[SLIDE]

Sometimes a block's going to be orphaned, which means that other people in the network come up with a longer chain.
So if they want to be sure that a payment's received, say before you hand over your house, people usually wait six blocks.

But we still need a way to choose which node gets to make that block. so here's the process.
Basically we want to pick a node at random, so that no one node gets to make all the blocks. 
In particular we don't want somebody to just be able to make up their own really long chain of blocks and rewrite history. 
So the solution in bitcoin is called proof-of-work. Let's take a look.

[SLIDE]

You take a hash of the last block, you pick a number, you make a hash of that number, and if it has enough zeros on the front, you win, and you have the right to make the next block. 
If it doesn't, you try again, and you keep trying until you find one. 
The point about this is that hardly any of the numbers you pick are going to result in a hash with enough zeros, so it takes a lot of work to solve this problem, but once you solve it and send the block to the rest of the network, it's very easy for them to check that the hash is right.

[SLIDE]

Anyway let's get back to content of the actual messages that we send across the network. 
But I'm going to look at it as a system for setting puzzles which confer the ability to set other puzzles.

So let me show you what I mean. Let's start with a game. I'm going to give you a problem, and if anybody here can solve the problem, they can set the next problem.

Problem
x + 3 = 5

Solution
x: 2

Get Problem
x * 5 = 25

[repeat 2 or 3 times]

Now, this is the basic process though which assets are controlled in bitcoin. 
You provide the solution to a problem, and that gives you the rights to set a new problem. 

[SLIDE]

Bitcoin expresses this in a little stack-based language, so let's translate these problems into a stack-based language and see what it looks like.

gforth

Now we're going to do this in a stack-based language. Who here has used forth or something 
similar? Let me fire up a forth interpreter and show you want I mean. 
So this is gnu forth, you can install it on ubuntu with 
apt-get install gforth.

Let's just type some junk in here and see what happens.
1
2
3
. 3 What do you think it'll do next?
. 2
. 1
. BOOM

There are some other operators apart from just "."
3
2
=

3
3
=

2 3 *

What happens here is we have two things going on, a stack and a script. The stack contains 
data, and we normally deal with whatevers on the top, which removes something from the stack 
and does something with it.

So let's see if we can restate the tranactions we just made in forth 

[slide back]

plug in scripts

[slide]

Let's have a look at a few of the opcodes bitcoin uses.

There's OP_TRUE. If you make a script just consisting of OP_TRUE then it's the equivalent of throwing money out of the window.

There's OP_FALSE. If you make a script just consisting of OP_False then nobody will ever be able to spend it. It's like the KLF burning a million pounds in some kind of vague artistic statement that they would later come to regret.

There's a thing called OP_RETURN to return. Nowadays I think it always returns false.
It was the cause of a bug in bitcoin where the solution script used to be literally tacked onto the beginning of the problem script     That got changed when someone realized you could provide OP_TRUE OP_RETURN as the solution to any problem..

...also the comparison op codes have a version with VERIFY stuck on the end which will return 
false if they're false rather than carrying on with the rest of the stack
1 2 EQUAL_VERIFY 1 2 3 

So as you can see this is a very useful technology for maths lessons, but it's also useful if 
you want a system to create 4 billion dollars and give them to free software hackers, so let 
me show you what a financial transaction might look like. Luckily as well as being able to 
add and divide bitcoin has some op codes for checking signatures.

[slide]

So there's a thing called OP_CHECKSIG
What it does is take a hash of the entire transaction, and see if the second thing on the 
stack - that's the signature - is valid for that hash with the public key specified as the 
first thing on the stack.

So let's take a look at an example transaction:

The simplest form looks like this:
scriptPubKey: [pubkey] OP_CHECKSIG
scriptSig: sig

So we set a puzzle which you could only solve if you had the right private key.

So the upshot of doing that is that you can give me your public key, 
I can make a script solved by that public key, and that effectively transfers control to you.


Now, there's another thing we'll need to be able to do if we want to use this as a payment system, which is that we need to be be able to manage how much each transaction is worth.

[slide]

That's easily done: Bitcoin has a "value" field along with each problem.
But the first problem is that you may have solved two different puzzles worth 10 and 15, and you want to somebody 25.

[slide]

So to do that we can feed in two solutions to two previous transactions into the same new transaction. 
tx abc
tx xyz


But we also need to handle the opposite problem, which what if you've got 25 and you only want to spend 10.

[slide]

So to do that, we can create multiple problems, or what we call outputs:
0 bob_pub OP_CHECKSIG    10
1 ed_pub OP_CHECKSIG     15
So that second one is the change.

BTW since we can have multiple outputs in one transaction that's going to affect how we deal with the inputs:
tx abc
tx xyz
...becomes:
tx abc:1
tx xyz:0

BTW you may be wondering what happens if the outputs don't match the inputs. 

First if we spend more than we have inputs for, the transaction is invalid.
But if we spend less than we have output for, the remainder is considered a fee.
You remember how all these guys are crunching away trying to find a hash with the right number of zeros, and occasionally they find it and they get to make a block.
At that point they'll also get all the unaccounted for fees in their transactions block. So that gives them an incentive to actually include our transaction in the block.

BTW if you're dealing with raw transactions you need to be a bit careful like this. 
Occasionally if you look at the block chain you see somebody's made a transaction with $100,000 in transaction fees.
That often represents somebody who had a really bad day.

[slide]

Anyhow let's just take a look at an actual transaction.
For legacy reasons the problem you set is called scriptPubKey, and the solution to the problem is called scriptSig, 

[slide]

Anyhow let's go back to the scripting system, because we're not yet done with a normal Bitcoin transaction.
Earlier we did this:
scriptPubKey: [pubkey] OP_CHECKSIG
scriptSig: sig

...and bitcoin could work that way for normal payments but what it actually does is a bit more 
involved:

ScriptPubKey: OP_DUP OP_HASH160 <hash of public key> OP_EQUALVERIFY OP_CHECKSIG
And your sigscript actually looks like this:
scriptSig: <sig> <public key>

So let's look at what happens.
We start with that on the stack
<sig> <pubKey>
...then op_dup to copy the top thing on the stack
<sig> <pubKey> <pubKey>
...then we hash the top thing and compare it to this hash, and if it fails return
<sig> <pubKey> op_checksig
...which is what we did last time.

So the reason for that whole palava was that we prefer to use the hash of a public key 
instead of an actual public key. Two reasons, firstly it's shorter. Secondly it's somewhat 
more secure, because even if somebody figure out a way to get your private key based on 
public key , they don't actually get to find out your public key until you actually use it to 
spend. Once you've used it once, if you then use it again you won't have that protection, but 
it's useful the first time you use any given key.

Anyhow that's the conventional way we'd use bitcoin, and a lot of client software will 
basically only really let you make these transactions. 

That's all you need to replace paypal or some money-transmitting intermediary.

[SLIDE]

BTW, we saw earlier when we started combining inputs that there's nothing in bitcoin that knows or cares who the inputs belong to.
A lot of the time you'll have a lot of different inputs that all have different signatures. 

So they don't have to be inputs from the same people.

So if you want two people to pay into one of these conditional transactions you can have them both sign inputs for the same transaction.
If one of them fails to sign, the deals off. 

Sometimes you want to sign before you know all the inputs. 
Normally you don't care who pays for the transaction as long as it's got enough payments.
The default signatures wouldn't sign the whole transaction, including all the inputs to it, but there's a special flag called 
SIGHASH_ANYONECANPAY
...which only signs the outputs not the inputs.

So let's imagine we want to buy a keg of beer for tlug which costs 10,000 yen, but only if 10 people chip in 1000 yen each.
We go to the Asahi website and get the address we've got to pay to, I guess they ship for bitcoins, if they don't, somebody should give them a call - 
And I start by making a transaction and signing it, with the Asahi address, and my 1000 yen.
Then instead of broadcasting it to the bitcoin network I can stick it up on the wiki.
Nobody can send it yet because the inputs aren't yet enough to cover the outputs.
The next person can add their inputs and sign, and so on until we've got enough, and then the 10th person can broadcast it.

Where this gets really interesting is when as well as just tracking one currency in bitcoin or another network, you start tracking other assets.
This is what Colorcoin and Ethereum and all those guys do. So now you can make these atomic trades, completely peer-to-peer, with no third-party, and nobody can cheat.

[SLIDE]

Now, we used OP_CHECKSIG there but there's also a thing called OP_CHECKMULTISIG

What this will allow you to do is to check that any given number of the signatures in that 
list is valid.
2 <K1> <K2> 2 CHECKMULTISIGVERIFY

So that allows you to make a transaction that can only be unlocked with 2 out of 2 
signatures. So maybe we've raised a lot of money at the auction today that's supposed to be 
spent on server hosting but we're worried that Alberto or Edward would run 
off with it. What we can do is when we donate the money we can do it like this:

2 <ALBERTO-PUBKEY> <EDWARD-PUBKEY> 2 CHECKMULTISIGVERIFY

So that way they both have to agree to spend the money.

That's all good but what happens if Edward loses his key? So 

maybe we'd set it up like this:
2 <ALBERTO-PUBKEY> <EDWARD-PUBKEY> <ZEV-PUBKEY> 3 CHECKMULTISIGVERIFY


So that was a way of locking up money between two people.
It could also be an escrow arrangement where one person is writing code for another person.

I should mention my venture, Reality Keys. 
We are a certificate authority for facts. 
You can go to our website and put in some proposition and we'll give you a pair of public keys, one for yes and one for no.
Then when that event happens we release the private key, and you can use that to sign whatever transactions you've made.

So you can do:
IF
2 <Alice> <Hillary Clinton Wins> 2 CHECKMULTISIG
ELSE
2 <Bob> <Hillary Clinton Loses> 2 CHECKMULTISIG
ENDIF

Then when the presidential election comes around we'll release one of these private keys 
based on what actually happens, and one person or the other can claim that transaction.

Now there is a bit of a hitch doing this stuff on the bitcoin network, which is that firstly 
the language is very limited and there are bunch of disabled opcodes that won't work at all. 
And secondly there are a bunch that are kind-of soft-disabled to prevent people hurting 
themselves with them. 

There's a check in the scripting engine called IsStandard, and what that does is to check a 
very limited set of script patterns. By default transactions that don't fit those patterns 
won't be relayed across the network and won't normally be mined and added into blocks. But 
there are a few generous people out there who will put those transactions in blocks for you, 

so you just have to submit them directly to them. It then takes a little bit of time for 
those people to get to mine a block.

[SLIDE]

Now, in those last couple of slides the problems that you have to solve were getting quite long and elaborate, while the solutions were just signatures.
There are some practical problems with doing things this way. 
If you're an online retailer you may want the payments you receive to be encumbered so that they need two signatures, but you don't want to have to get the customers sending you the money to make the appropriate script to do that. 
So there's a slightly hacky solution to deal with that called Pay 2 Script Hash.
The idea is that when you set the problem, instead of providing the whole script, you just provide a hash of the script.
When the system sees this particular pattern it drops what it's doing and does something weird.
namely getting a script from the solution script and executing that instead.

What that means in practice is that I can send you a hash of the script that I want my funds to be encumbered with, and you can send funds to that just like it's a normal bitcoin address.

[SLIDE]

So that's the basic idea. 
AFAIK it's the first time in history we've been able to do this. The payment uses of 
bitcoin where you transfer value from one person to another is pretty much a reversion to the 
status quo before the invention of the telegram. Cash was a fully decentralized system. 

Nobody can censor your transactions, if I want to give you 100 yen, there's nothing the Bank 
of Japan can do to stop me. 

But then we got the internet which allows us to transact over a distance, 
but our cash system didn't work on the internet.
So we had these gruesome hacks involving giving your money to a intermediary like PayPal.

With bitcoin payments we take out that intermediary, it's is pretty much a reversion 
to traditional payment infrastructure. Having to transact with PayPal or a bank or somebody 
in the middle is a fairly recent development. 

But this stuff is new. 
There are all kinds of areas where if you wanted payment to be 
conditional on some set of facts or some combination of agreements, you've always had to have 
some trusted guy in the middle. 

Typically that trusted person looks after your assets and 
every now and again they turn out not to be trustworthy and lose them or run off with them.

Alternatively you make a legal contract and you have some guy evaluate that contract, 
And the legal system has root access to everybody's stuff.

In a lot of cases we don't need that any more, which I think is going to be interesting.


